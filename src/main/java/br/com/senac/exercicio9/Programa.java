
package br.com.senac.exercicio9;


public class Programa {
    
    public static final double MEDIA = 12;
    
    
    double distancia;
    double litrosUsados;

    public Programa(){
    }
    
    public double calculoDistancia(double tempo , double velocidade){
        this.distancia = tempo * velocidade;
        return distancia;
    }
    
    public double litrosUsados(double tempo, double velocidade){
        double resultado = calculoDistancia(tempo, velocidade) / MEDIA;
        return  resultado;
    }

    
    
}
