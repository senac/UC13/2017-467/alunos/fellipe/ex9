package br.com.senac.exercicio9;

import org.junit.Test;
import static org.junit.Assert.*;

public class Exercicio9Teste {

    public Exercicio9Teste() {
    }

    @Test
    public void CalcularDistancia() {
        Programa calculoDistancia = new Programa();
        double resultado = calculoDistancia.calculoDistancia(2, 100);
        assertEquals(200, resultado, 0.01);
    }

    @Test
    public void CalcularQuantidadeLitros() {
        Programa calculoQuantLitros = new Programa();
        double resultado = calculoQuantLitros.litrosUsados(2, 120);
        assertEquals(20, resultado, 0.01);
    }

}
